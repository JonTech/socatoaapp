package com.example.socatoaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.sql.ResultSet;

public class MainActivity extends AppCompatActivity {




    String calcMode = "NONE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView catView = findViewById(R.id.cateto);
        final TextView hypView = findViewById(R.id.hipotenusa);
        final TextView resView = findViewById(R.id.angulo);
        final TextView infoView = findViewById(R.id.systemInfo);

        Button sinBtn = findViewById(R.id.sinBtn);
        sinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                catView.setHint("Cateto Opuesto");
                hypView.setHint("Hipotenusa");
                calcMode = "SIN";
            }
        });
        Button cosBtn = findViewById(R.id.cosBtn);
        cosBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                catView.setHint("Cateto Adjacente");
                hypView.setHint("Hipotenusa");
                calcMode = "COS";
            }
        });
        Button tanBtn = findViewById(R.id.tanBtn);
        tanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                catView.setHint("Cateto Adjacente");
                hypView.setHint("Cateto Opuesto");
                calcMode = "TAN";
            }
        });
        Button calcBtn = findViewById(R.id.calcBtn);
        calcBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (calcMode.equals("NONE"))
                {
                    infoView.setText("[ERROR] Mode was not set!");
                    infoView.setVisibility(View.VISIBLE);
                } else {
                    calculate();
                }
            }
        });

    }

    /**
     *  Gets the data from the UI to operate
     * */
    public double[] getData() {


        TextView catView = findViewById(R.id.cateto);
        TextView hypView = findViewById(R.id.hipotenusa);
        TextView resView = findViewById(R.id.angulo);

        double[] res = new double[2];

        String arg1 = catView.getText().toString();
        String arg2 = hypView.getText().toString();

        if (arg1.equals("") || arg2.equals(""))
        {
            //resView.setText("[ERROR] Missing value!");
            return null;
        }

        res[0] = Double.parseDouble(arg1);
        res[1] = Double.parseDouble(arg2);

        return res;

    }
    /**
     *  Writes the result into the TextView element
     * */
    public void setData(double... args) {

        TextView catView = findViewById(R.id.cateto);
        TextView hypView = findViewById(R.id.hipotenusa);
        TextView resView = findViewById(R.id.angulo);
        TextView res2View = findViewById(R.id.grados);
        /*
        for (Double d : args) {
            System.out.println("Input was: " + d);
        }*/
        resView.setText("Result is: " + args[0]+"");
        res2View.setText("Degrees is: " + args[1]+"");
    }


    public void calculate() {
        switch(calcMode)
        {
            case("SIN"):
            {
                calcSin(getData());
                break;
            }
            case("COS"):
            {
                calcCos(getData());
                break;
            }
            case("TAN"):
            {
                calTan(getData());
                break;
            }
        }
    }

    public void calcSin(double... args) {

        TextView infoView = findViewById(R.id.systemInfo);

        if (args == null)
        {
            infoView.setText("[ERROR] Missing values!");
            infoView.setVisibility(View.VISIBLE);
            return;
        }

        double degrees;
        double angle;

        angle = (args[0]/args[1]);
        degrees = Math.toDegrees(Math.asin(angle));

       System.out.println("SIN is: " + degrees);

        setData(angle, degrees);

    }
    public void calcCos(double... args) {

        double degrees;
        double angle;

        TextView infoView = findViewById(R.id.systemInfo);

        if (args == null)
        {
            infoView.setText("[ERROR] Missing values!");
            infoView.setVisibility(View.VISIBLE);
            return;
        }

        angle = (args[1]/args[0]);
        degrees = Math.toDegrees(Math.acos(angle));

        System.out.println("COS is: " + degrees);

        setData(angle, degrees);

    }
    public void calTan(double... args) {

        double degrees;
        double angle;

        TextView infoView = findViewById(R.id.systemInfo);

        if (args == null)
        {
            infoView.setText("[ERROR] Missing values!");
            infoView.setVisibility(View.VISIBLE);
            return;
        }

        angle = (args[0]/args[1]);
        degrees = Math.toDegrees(Math.atan(angle));

        System.out.println("TAN is: " + degrees);

        setData(angle, degrees);

    }




}
